package main

import (
	"fmt"
)

const (
	first = iota
	second
	third
)

const (
	fourth = iota
	fifth  = 1 << (10 * iota)
)

func main() {
	var myInt = 42
	fmt.Println(myInt)

	var myfloat = 42.
	fmt.Print(myfloat)

	myString := "Hello Go"
	fmt.Println(myString)

	myComplex := complex(2, 3)
	fmt.Println(myComplex)
	fmt.Println(real(myComplex))
	fmt.Println(imag(myComplex))

	fmt.Println(first)
	fmt.Println(second)
	fmt.Println(third)
	fmt.Println(fourth)
	fmt.Println(fifth)

	myArray := [...]int{42, 15, 23}
	fmt.Println(myArray)
	fmt.Println(len(myArray))

	mySlice := myArray[:]
	mySlice = append(mySlice, 100)
	fmt.Println(mySlice)

	newSlice := make([]float32, 100)
	newSlice[0] = 1
	newSlice[1] = 7834

	myMap := make(map[int]string)

	fmt.Println(myMap)

	myMap[22] = "Hi"
	myMap[14] = "Yo"

	fmt.Println(myMap)
	fmt.Println(myMap[99])
}
