package main

import (
	"errors"
	"fmt"
	"time"
)

func main() {
	po := new(PurchaseOrder)
	po.Value = 42.27

	savePO(po, false).Then(func(obj interface{}) error {
		po := obj.(*PurchaseOrder)
		fmt.Printf("Purchase Order saved with ID: %d\n", po.Number)
		return nil
	}, func(err error) {
		fmt.Printf("Failed to save the purchase order: " + err.Error() + "\n")
	}).Then(func(obj interface{}) error {
		fmt.Println("I can chain promises!")
		return nil
	}, func(err error) {
		fmt.Println("Oops! seems that I can't chain promises :/")
	})
	//just to let everything execute
	fmt.Scanln()
}

type PurchaseOrder struct {
	Number int
	Value  float64
}

func savePO(po *PurchaseOrder, shouldFail bool) *Promise {
	result := new(Promise)

	result.successChannel = make(chan interface{}, 1)
	result.failureChannel = make(chan error, 1)

	go func() {
		//this triggers a timeout error on purpose, uncomment for fun :D
		// time.Sleep(2 * time.Second)
		if shouldFail {
			result.failureChannel <- errors.New("Something failed in the purchase order")
		} else {
			po.Number = 1234
			result.successChannel <- po
		}
	}()
	return result
}

type Promise struct {
	successChannel chan interface{}
	failureChannel chan error
}

func (promise *Promise) Then(success func(interface{}) error, failure func(error)) *Promise {
	result := new(Promise)

	result.successChannel = make(chan interface{}, 1)
	result.failureChannel = make(chan error, 1)

	timeout := time.After(1 * time.Second)
	//this will alow us to return the new promise synchronously while it's actually being processed asynchronously
	go func() {
		select {
		case obj := <-promise.successChannel:
			newErr := success(obj)
			if newErr == nil {
				result.successChannel <- obj
			} else {
				result.failureChannel <- newErr
			}
		case err := <-promise.failureChannel:
			failure(err)
			result.failureChannel <- err
		case <-timeout:
			failure(errors.New("Timeout exceeded!"))
		}
	}()
	return result
}
