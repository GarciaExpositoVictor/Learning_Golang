package main

//Simulates a purchase order assuming that it takes a long call to a database or webservice and we have to wait for it to resolve
import (
	"fmt"
)

func main() {
	po := new(PurchaseOrder)
	po.Value = 42.27

	ch := make(chan *PurchaseOrder)

	go savePO(po, ch)

	newPo := <-ch
	fmt.Printf("PO: %v", newPo)
}

type PurchaseOrder struct {
	Number int
	Value  float64
}

func savePO(po *PurchaseOrder, callback chan *PurchaseOrder) {
	po.Number = 1234

	callback <- po
}
