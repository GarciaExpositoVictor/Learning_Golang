package main

import (
	"fmt"
)

func main() {
	btn := makeButton()

	handlerOne := make(chan string)
	handlerTwo := make(chan string)

	btn.addEventListener("click", handlerOne)
	btn.addEventListener("click", handlerTwo)

	go func() {
		for {
			msg := <-handlerOne
			fmt.Println("Handler One: " + msg)
		}
	}()

	go func() {
		for {
			msg := <-handlerTwo
			fmt.Println("Handler Two: " + msg)
		}
	}()

	btn.triggerEvent("click", "Button clicked")

	btn.removeEventListener("click", handlerTwo)

	btn.triggerEvent("click", "Button clicked again")

	//this is just to make the process wait until all the goroutines have had a chance to run
	fmt.Scanln()
}

type Button struct {
	eventListeners map[string][]chan string
}

func makeButton() *Button {
	result := new(Button)
	result.eventListeners = make(map[string][]chan string)
	return result
}

func (button *Button) addEventListener(event string, responseChannel chan string) {
	if _, present := button.eventListeners[event]; present {
		button.eventListeners[event] = append(button.eventListeners[event], responseChannel)
	} else {
		button.eventListeners[event] = []chan string{responseChannel}
	}
}

func (button *Button) removeEventListener(event string, listenerChannel chan string) {
	if _, present := button.eventListeners[event]; present {
		for idx, _ := range button.eventListeners[event] {
			if button.eventListeners[event][idx] == listenerChannel {
				button.eventListeners[event] = append(button.eventListeners[event][:idx],
					button.eventListeners[event][idx+1:]...)
				break
			}
		}
	}
}

func (button *Button) triggerEvent(event string, response string) {
	if _, present := button.eventListeners[event]; present {
		for _, handler := range button.eventListeners[event] {
			go func(handler chan string) {
				handler <- response
			}(handler)
		}
	}
}
