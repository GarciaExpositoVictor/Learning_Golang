package main

import (
	"encoding/csv"
	"io/ioutil"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"
)

const watchedPath = "./src"

func main() {
	runtime.GOMAXPROCS(4)
	for {
		d, _ := os.Open(watchedPath)
		//Negative index so it reads all files it finds. A positive number would set a limit on this function
		files, _ := d.Readdir(-1)
		for _, file := range files {
			filePath := watchedPath + "/" + file.Name()
			f, _ := os.Open(filePath)
			data, _ := ioutil.ReadAll(f)
			f.Close()
			os.Remove(filePath)

			go func(data string) {
				reader := csv.NewReader(strings.NewReader(data))
				records, _ := reader.ReadAll()
				for _, r := range records {
					invoice := new(Invoice)
					invoice.Number = r[0]
					invoice.Amount, _ = strconv.ParseFloat(r[1], 64)
					invoice.PurchaseOrderNumber, _ = strconv.Atoi(r[2])
					unixTime, _ := strconv.ParseInt(r[3], 10, 64)
					invoice.InvoiceDate = time.Unix(unixTime, 0)
				}
			}(string(data))
		}
	}
}

//Invoice struct
type Invoice struct {
	Number              string
	Amount              float64
	PurchaseOrderNumber int
	InvoiceDate         time.Time
}
